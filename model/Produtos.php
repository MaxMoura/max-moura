<?php
/**
 * Created by PhpStorm.
 * Autor: Max Bilio
 * Date: 24/10/2018
 * Time: 02:50
 */

/*** ======= Descricao do Produtos ======= */
class Produtos extends Conexao
{
    private $pro_descricao, $pro_status;

    function __construct()
    {
        parent::__construct();
    }

    /*** ======= Busca  todos os produtos sem filtrar ======= */
    function GetProdutos()
    {
        $query = "SELECT * FROM {$this->prefix}clientes_produtos p INNER JOIN {$this->prefix}clientes c ON p.pro_id = c.cli_id_produto";
        $query .= " ORDER BY pro_id DESC";

        $this->ExecuteSQL($query);

        $this->GetLista();
    }

    /*** ======= @param INT id do produto ======= */
    function GetProdutosID($id)
    {
        $id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);

        $query = "SELECT * FROM {$this->prefix}clientes_produtos p INNER JOIN {$this->prefix}clientes c ON p.pro_id = c.cli_id_produto";
        $query .= " AND pro_id = :id";

        $params = array(':id' => (int)$id);

        $this->ExecuteSQL($query, $params);

        $this->GetLista();
    }

    /** ===== ===== ===== @param INT  id da cliente ===== ===== ===== */
    function GetProdutosCli($id)
    {
        $id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);

        $query = "SELECT * FROM {$this->prefix}clientes_produtos p INNER JOIN {$this->prefix}clientes c ON p.pro_id = c.cli_id_produto";
        $query .= " AND pro_id = :id ";

        $params = array(':id' => (int)$id);

        $this->ExecuteSQL($query, $params);

        $this->GetLista();
    }


    /** ===== ===== ===== retorna o array com os itens da tabela ===== ===== ===== */
    private function GetLista()
    {
        $i = 1;
        while ($lista = $this->ListarDados()):

            $this->itens[$i] = array(
                'pro_id' => $lista['pro_id'],
                'pro_descricao' => $lista['pro_descricao'],
                'pro_status' => $lista['pro_status'],
            );

            $i++;

        endwhile;
    }

    /** ===== ===== ===== Preparando todos os campos antes de gravar no banco ===== ===== ===== */
    function Preparar($pro_descricao, $pro_status)
    {
        $this->setProDescricao($pro_descricao);
        $this->setProStatus($pro_status);
    }

    /** ===== ===== ===== insere os dados no banco apos terem sido preparados ===== ===== ===== */
    function Inserir($id)
    {
        $query = "INSERT INTO {$this->prefix}clientes_produtos(pro_descricao, pro_status)";
        $query .= " VALUES ";
        $query .= " (:pro_descricao, :pro_status)";

        $params = array(
            '::pro_descricao' => $this->getProDescricao(),
            ':pro_status' => $this->getProStatus(),
            ':pro_id' => (int)$id,
        );

        // executo a SQL
        if ($this->ExecuteSQL($query, $params)):

            return TRUE;

        else:

            return FALSE;

        endif;
    }

    /** ===== Param int $id do produto que vai apagar ===== */
    function Apagar($id)
    {
        $query = "DELETE FROM {$this->prefix}clientes_produtos";
        $query .= " WHERE pro_id= :id";

        $params = array(':id' => (int)$id);

        if ($this->ExecuteSQL($query, $params)):
            return TRUE;
        else:
            return FALSE;
        endif;
    }

    /** ===== Método que altera  um produto existente ===== */
    function Alterar($id)
    {
        $query = "UPDATE {$this->prefix}clientes_produtos SET pro_descricao=:pro_descricao, pro_status=:pro_status)";
        $query .= " WHERE pro_id = :pro_id";

        $params = array(
            '::pro_descricao' => $this->getProDescricao(),
            ':pro_status' => $this->getProStatus(),
            ':pro_id' => (int)$id,
        );

        // executo a SQL
        if ($this->ExecuteSQL($query, $params)):

            return TRUE;

        else:

            return FALSE;

        endif;
    }

    /** ===== ===== ===== Métodos Getts ===== ===== ===== */
    // @return mixed */
    public function getProDescricao()
    {
        return $this->pro_descricao;
    }

    /*** @return mixed */
    public function getProStatus()
    {
        return $this->pro_status;
    }

    /** ===== ===== ===== Métodos Setts ===== ===== ===== */

    /** @param mixed $pro_descricao */
    public function setProDescricao($pro_descricao)
    {
        $this->pro_descricao = $pro_descricao;
    }

    /*** @param mixed $pro_status */
    public function setProStatus($pro_status)
    {
        $this->pro_status = $pro_status;
    }

}

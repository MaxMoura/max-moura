<?php
/**
 * Created by PhpStorm.
 * Autor: Max Bilio
 * Date: 23/10/2018
 * Time: 10:58
 */

class Rotas
{
    /**
     * Trata paginas e paramentros da URl
     * @var string - define a pasta controller
     * @var string - define a pasta view
     */
    private static $pasta_controller = 'controller';
    private static $pasta_view = 'view';

    /**
     * @var array : recebe os parametros da URL
     */
    public static $pag;

    static function get_Pagina()
    {

        // Verifico se passou paramentros na URL
        if (isset($_GET['pag'])):

            $pagina = $_GET['pag'];

            // separa a URL pela barra e gera os parametros
            self::$pag = explode('/', $pagina);

            // DIRECTORY_SEPARATOR é para criar barra de acordo com o SO
            $barra = DIRECTORY_SEPARATOR;

            $pagina = self::$pasta_controller . $barra . self::$pag[0] . '.php';

            // Verifico se existe pagina com nome passado na URL
            if (file_exists($pagina)):
                include $pagina;

            // se não existe o arquivo mostra erro
            else:
                echo 'Arquivo não encontrado :' . $pagina;
                include 'erro.php';
            endif;
        else:
            include 'home.php';

        endif;

    }

    /**@return string: URL home do site */
    public static function get_SiteHOME()
    {
        return Config::SITE_URL . '/' . Config::SITE_PASTA;
    }

    static function get_SiteRAIZ()
    {
        return $_SERVER['DOCUMENT_ROOT'] . '/' . Config::SITE_PASTA;
    }

    static function get_SiteTEMA()
    {
        return self::get_SiteHOME() . '/' . self::$pasta_view;
    }

    /** Retorna uma string com a pasta de controle */
    static function get_Pasta_Controller()
    {
        return self::$pasta_controller;

    }

    /** Retorna string tela de cadastro */
    static function pag_ClienteCadastro()
    {
        return self::get_SiteHOME() . '/cadastro';
    }

    /** ===== retorno página com dados do cliente =====*/
    static function pag_ClienteAlterar()
    {
        return self::get_SiteHOME() . '/clientes_alterar';
    }

    /** ===== @return string - pagina de cliente dados ===== */
    static function pag_ClienteDados()
    {
        return self::get_SiteHOME() . '/cliente_dados';
    }

    /** ===== @return string - pagina de produtos ===== */
    static function pag_Produtos()
    {
        return self::get_SiteHOME() . '/produtos';
    }

}
<?php
/**
 * Created by PhpStorm.
 * Autor: Max Bilio
 * Date: 23/10/2018
 * Time: 09:46
 */

/**
 * descricao Config
 * armazena diversas informações do sistema
 */
class Config
{
    /*** ======== INFORMAÇÕES DE BANCO DE DADOS ======== */

    const BD_HOST = "localhost";
    const BD_USER = "root";
    const BD_SENHA = "";
    const BD_BANCO = "testejr";
    const BD_PREFIXE = "as_";

    /**
     * ================== INFORMAÇÕES DO SITE ==================
     * Url do site
     * Pasta padrao do site
     * Nome do site
     * Email do administrador do site
     */
    //const SITE_URL = "http://maxmoura.com.br";
    const SITE_URL = "http://localhost";
    const SITE_PASTA = "max-moura";
    const SITE_NOME = "Teste Pratico";
    const SITE_EMAIL_ADM = "biliorx@gmail.com";

}
<?php
/**
 * Created by PhpStorm.
 * Autor: Max Bilio
 * Date: 23/10/2018
 * Time: 09:45
 */

class Clientes extends Conexao
{
    private $cli_nome, $cli_data_nascimento, $cli_id_produto, $cli_status;

    /** ===== Chamando o método construtor da classe ===== */
    function __construct()
    {
        parent::__construct();
    }

    /**==== busca todos os clientes ====*/
    function GetClientes()
    {
        $query = "SELECT * FROM {$this->prefix}clientes";
        $this->ExecuteSQL($query);

        $this->GetLista();
    }

    /** ====== @param INT $id id do cliente ====== */
    function GetClientesID($id)
    {

        // monto a SQL
        $query = " SELECT * FROM {$this->prefix}clientes ";
        $query .= " WHERE cli_id = :id ";
        // passo parametros
        $params = array(':id' => (int)$id);
        //executo a SQL
        $this->ExecuteSQL($query, $params);
        // chamo a listagem
        $this->GetLista();

    }

    /*** ====== fazendo a listagem dos dados retornados ====== */
    private function GetLista()
    {

        $i = 1;
        while ($lista = $this->ListarDados()):

            $this->itens[$i] = array(

                'cli_id' => $lista['cli_id'],
                'cli_nome' => $lista['cli_nome'],
                'cli_data_nascimento' => $lista['cli_data_nascimento'],
                'cli_id_produto' => $lista['cli_id_produto'],
                'cli_status' => $lista['cli_status'],
            );

            $i++;

        endwhile;

    }

    /** ======= Prepara os campos para inserir ou para atualizar ======= */
    function Preparar($cli_nome, $cli_data_nascimento, $cli_id_produto, $cli_status)
    {
        $this->setCliNome($cli_nome);
        $this->setCliDataNascimento($cli_data_nascimento);
        $this->setCliIdProduto($cli_id_produto);
        $this->setCliStatus($cli_status);
    }

    /** ======= Inseri os dados no banco de dados ======= */
    function Inserir()
    {
        $query = "INSERT INTO {$this->prefix}clientes (cli_nome,cli_data_nascimento, cli_id_produto, cli_status)";
        $query .= " VALUES ";
        $query .= "(:cli_nome, :cli_data_nascimento, :cli_id_produto, :cli_status)";

        $params = array(
            ':cli_nome' => $this->getCliNome(),
            ':cli_data_nascimento' => $this->getCliDataNascimento(),
            ':cli_id_produto' => $this->getCliIdProduto(),
            ':cli_status' => $this->getCliStatus()
        );

        $this->ExecuteSQL($query, $params);

    }

    /** ===== ===== ===== Alterar cliente no banco ===== ===== ===== */
    function Alterar($id)
    {
        $query = " UPDATE {$this->prefix}clientes SET (cli_nome=:cli_nome,cli_data_nascimento=:cli_data_nascimento, cli_id_produto=:cli_id_produto, cli_status:cli_status)";
        $query .= " WHERE cli_id = :cli_id";

        $params = array(
            ':cli_nome' => $this->getCliNome(),
            ':cli_data_nascimento' => $this->getCliDataNascimento(),
            ':cli_id_produto' => $this->getCliIdProduto(),
            ':cli_status' => $this->getCliStatus(),
            ':cli_id' => (int)$id
        );

        if ($this->ExecuteSQL($query, $params)):

            return true;

        else:

            return false;
        endif;
    }

    /** ===== ===== ===== Deleta cliente no banco ===== ===== ===== */
    function Apagar($id)
    {
        $query = "DELETE FROM {$this->prefix}clientes";
        $query .= " WHERE cli_id= :id";

        $params = array(':id' => (int)$id);

        if ($this->ExecuteSQL($query, $params)):
            return TRUE;
        else:
            return FALSE;
        endif;
    }


    /** ===== ===== ===== Métodos Getts ===== ===== ===== */
    // @return mixed */
    public function getCliNome()
    {
        return $this->cli_nome;
    }

    /*** @return mixed */
    public function getCliDataNascimento()
    {
        return $this->cli_data_nascimento;
    }

    /*** @return mixed */
    public function getCliIdProduto()
    {
        return $this->cli_id_produto;
    }

    /*** @return mixed */
    public function getCliStatus()
    {
        return $this->cli_status;
    }

    /** ===== ===== ===== Métodos Setts ===== ===== ===== */

    /*** @param mixed $cli_nome */
    public function setCliNome($cli_nome)
    {
        $this->cli_nome = $cli_nome;
    }

    /*** @param mixed $cli_data_nascimento */
    public function setCliDataNascimento($cli_data_nascimento)
    {
        $this->cli_data_nascimento = $cli_data_nascimento;
    }

    /*** @param mixed $cli_id_produto */
    public function setCliIdProduto($cli_id_produto)
    {
        $this->cli_id_produto = $cli_id_produto;
    }

    /*** @param mixed $cli_status */
    public function setCliStatus($cli_status)
    {
        $this->cli_status = $cli_status;
    }

}
<?php
/**
 * Created by PhpStorm.
 * Autor: Max Bilio
 * Date: 23/10/2018
 * Time: 09:46
 */

class Conexao extends Config
{
    private $host, $user, $senha, $banco;
    protected $obj;
    protected $itens = array();
    protected $prefix;

    /** ======== método construtor ======== */
    function __construct()
    {
        $this->host = self::BD_HOST;
        $this->user = self::BD_USER;
        $this->senha = self::BD_SENHA;
        $this->banco = self::BD_BANCO;

        $this->prefix = self::BD_PREFIXE;

        try {

            if ($this->Conectar() == null):
                $this->Conectar();

            endif;

        } catch (Exception $e) {

            exit($e->getMessage() . '<h2>Ops... Erro no banco de dados, tente novamente mais tarde. </h2>');
        }
    }

    /**
     * Faz a conexão propriamente dita
     * @return \PDO link com dados da conexão
     */
    private function Conectar()
    {
        /** Aqui é para garantir que tudo que digitr no banco será formatado */
        $options = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
        , PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING
        );

        $link = new PDO("mysql:host={$this->host}; dbname={$this->banco}",
            $this->user, $this->senha, $options);
        return $link;
    }

    /** ========= @return PDO uma consulta ========= */
    function ExecuteSQL($query, array $params = NULL)
    {
        $this->obj = $this->Conectar()->prepare($query);

        // contagem dos elementos do aray params
        if (count($params) > 0) {

            // varrendo o array e pegando os dados
            foreach ($params as $key => $value):

                $this->obj->bindValue($key, $value);

            endforeach;
        }
        return $this->obj->execute();
    }

    /*** @return array com dados da SQL */
    function ListarDados()
    {

        return $this->obj->fetch(PDO::FETCH_ASSOC);
    }

    /*** @return int com total de registros */
    function TotalDados()
    {
        return $this->obj->rowCount();
    }

    function GetItens()
    {
        return $this->itens;
    }

}
<?php
/**
 * Created by PhpStorm.
 * Autor: Max Bilio
 * Date: 23/10/2018
 * Time: 11:13
 */

class Template extends SmartyBC
{
    function __construct()
    {
        parent::__construct();

        $this->setTemplateDir('view/');
        $this->setCompileDir('view/compile/');
        $this->setCacheDir('view/canche/');
    }

}
-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 22-Out-2018 às 12:38
-- Versão do servidor: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `testejr`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `clientes`
--

DROP TABLE IF EXISTS `clientes`;
CREATE TABLE IF NOT EXISTS `clientes` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NOME` varchar(250) NOT NULL,
  `DATA_NASCIMENTO` date NOT NULL,
  `ID_PRODUTO` int(11) NOT NULL,
  `STATUS` int(1) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `clientes`
--

INSERT INTO `clientes` (`ID`, `NOME`, `DATA_NASCIMENTO`, `ID_PRODUTO`, `STATUS`) VALUES
(1, 'João', '1986-10-10', 1, 1),
(2, 'Flavio', '1986-05-06', 3, 1),
(3, 'Gustavo', '1986-05-06', 4, 1),
(4, 'Diego', '1980-05-06', 2, 1),
(5, 'Vitor', '1979-03-25', 1, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `clientes_produtos`
--

DROP TABLE IF EXISTS `clientes_produtos`;
CREATE TABLE IF NOT EXISTS `clientes_produtos` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DESCRICAO` varchar(250) NOT NULL,
  `STAUS` int(1) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `clientes_produtos`
--

INSERT INTO `clientes_produtos` (`ID`, `DESCRICAO`, `STAUS`) VALUES
(1, 'PRODUTO 1', 1),
(2, 'PRODUTO 2', 1),
(3, 'PRODUTO 3', 1),
(4, 'PRODUTO 4', 1),
(5, 'PRODUTO 5', 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

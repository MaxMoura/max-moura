<?php
/**
 * Created by PhpStorm.
 * Autor: Max Bilio
 * Date: 23/10/2018
 * Time: 11:23
 */


/** === Esse arquivo autoload é quem vai carregar as classes através da index === */
require './lib/autoload.php';

// Chamo a classe do template
$smarty = new Template();

$cliente = new Clientes();


/** ====== Passo valores para o meu template ======*/
$smarty->assign('GET_TEMA', Rotas::get_SiteTEMA());
$smarty->assign('GET_SITE_HOME', Rotas::get_SiteHOME());
$smarty->assign('SITE_NOME', Config::SITE_NOME);


$smarty->assign('CATEGORIAS', $cliente->GetItens());
$smarty->assign('PAG_CADASTRO', Rotas::pag_ClienteCadastro());
$smarty->assign('PAG_CLIENTE_DADOS', Rotas::pag_ClienteDados());
$smarty->assign('PAG_PRODUTOS', Rotas::pag_Produtos());
$smarty->assign('PAG_CLIENTE_ALTERAR', Rotas::pag_ClienteAlterar());

/** ====== Chamo a minha index tpl ======*/
$smarty->display('index.tpl');
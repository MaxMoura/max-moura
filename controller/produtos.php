<?php
/**
 * Created by PhpStorm.
 * Autor: Max Bilio
 * Date: 24/10/2018
 * Time: 03:36
 */

/*** ======= Chamo o objeto template ======= */
$smarty = new Template();
// objeto produtos
$produtos = new Produtos();

// verifico se passei ID de cliente, se passei mostro itens de cliente
if (isset(Rotas::$pag[1])):
    $produtos->GetProdutosCli((int)Rotas::$pag[1]);

else:
    /* se nao passei mostro  tudo */
    $produtos->GetProdutos();
endif;


/*** ======= passo variavies para o template TPL ======= */
$smarty->assign('PRO', $produtos->GetItens());
$smarty->assign('PRO_TOTAL', $produtos->TotalDados());


/*** ======= chamo o template ======= */
$smarty->display('produtos.tpl');
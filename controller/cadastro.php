<?php
/**
 * Created by PhpStorm.
 * Autor: Max Bilio
 * Date: 23/10/2018
 * Time: 09:36
 */

/** ===== Chamo o meu objeto template ===== */
$smarty = new Template();

$smarty->assign('PAG_CADASTRO', Rotas::pag_ClienteCadastro());


// verifico se passou os POSTS
if (isset($_POST['cli_nome']) and isset($_POST['cli_data_nascimento'])):

    // criando variaveis
    $cli_nome = $_POST['cli_nome'];
    $cli_data_nascimento = $_POST['cli_data_nascimento'];
    $cli_id_produto = $_POST['cli_id_produto'];
    $cli_status = $_POST['cli_status'];

    // gravo os dados no banco
    $clientes = new Clientes();
    $clientes->Preparar($cli_nome, $cli_data_nascimento, $cli_id_produto, $cli_status);

    $clientes->Inserir();

    // passo variaveis para o template de email de cadastro realizado
    $smarty->assign('SITE', Config::SITE_NOME);
    $smarty->assign('SITE_HOME', Rotas::get_SiteHOME());

    // vefico cadastro e dou ,aviso e levo o cliente até o login
    echo '<div class="alert alert-success">Olá ' . $cli_nome . ', Cadastro efetuado com sucesso!</div>';

    // se não tejm POSTS  mostra  os campos do cadastro
else:

    /** ===== Chamo o meu template ===== */
    $smarty->display('cadastro.tpl');

endif;

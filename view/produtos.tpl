<h3>Lista de produtos</h3>
<hr>

<!-- exibe mensagem caso nao encontre produtos --->
{if $PRO_TOTAL < 1}
    <h4 class="alert alert-danger">Ops... Nada foi encontrado </h4>
{/if}

<section id="pagincao" class="row">
    <center>
        {$PAGINAS}
    </center>
</section>

<!--  começa lista de produtos  ---->
<section id="produtos" class="row">

    <ul style="list-style: none">

        <li class="col-md-4">

            <div class="thumbnail">

                <a href="{$P.pro_id}">

                    <div class="caption">

                        <h4 class="text-center">Descrição: {$P.pro_pro_descricao} </h4>

                        <h3 class="text-center text-danger">Status: {$P.pro_status}</h3>

                    </div>

                </a>

            </div>

        </li>

    </ul>

</section>
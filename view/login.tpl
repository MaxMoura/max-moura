<h4>Login</h4>

{if $LOGADO == true}

{else}
    <section class="row" id="fazerlogin">

        <form name="cliente_login" method="post" action="">

            <div class="col-md-4"> </div>

            <!-- aqi estão os campos  -->
            <div class="col-md-4">

                <div class="form-group">

                    <label>Email:</label>
                    <input type="email" class="form-control input-lg" name="txt_email" value=""
                           placeholder="Digite seu email" required>
                </div>

                <div class="form-group">

                    <label>Senha:</label>
                    <input type="password" class="form-control input-lg" name="txt_senha" value=""
                           placeholder="Digite sua senha" required>
                </div>

                <div class="form-group">
                    <button class="btn btn-geral btn-block"><i class="glyphicon glyphicon-log-in"></i> Entrar</button>
                </div>
                <div class="form-group">

                    <a href="#" class="btn btn-success"><i class="glyphicon glyphicon-pencil"></i> Me
                        Cadastrar</a>
                    <a href="#" class="btn btn-danger"><i
                                class="glyphicon glyphicon-question-sign"></i>
                        Esqueci a Senha</a>

                </div>

            </div>

            <div class="col-md-4"></div>


        </form>

    </section>
{/if}
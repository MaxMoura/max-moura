<!DOCTYPE html>

<html>
<head>
    <title>{$SITE_NOME}</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="{$GET_TEMA}/tema/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <script src="{$GET_TEMA}/tema/js/jquery-2.2.1.min.js" type="text/javascript"></script>
    <script src="{$GET_TEMA}/tema/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- meu aquivo pessoal de CSS-->
    <link href="{$GET_TEMA}/tema/css/tema.css" rel="stylesheet" type="text/css"/>

</head>
<body>

<!-- começa  o container geral -->
<div class="container-fluid">

    <!-- começa a div topo -->
    <div class="row" id="topo">


    </div><!-- fim da div topo -->

    <!-- começa a barra MENU-->
    <div class="row" id="barra-menu">

        <!--começa navBAR-->
        <nav class="navbar navbar-inverse">

            <!-- container navBAr-->
            <div class="container">
                <!-- header da navbar-->
                <div class="navbar-header">
                    <!-- botao que mostra e esconde a navbar-->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                            aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                </div><!--fim header navbar-->

                <div class="collapse navbar-collapse" id="navbar">
                    <ul class="nav navbar-nav">
                        <li><a href="{$GET_SITE_HOME}"><i class="glyphicon glyphicon-home"></i> Home </a></li>

                        <li><a href="{$PAG_CADASTRO}"><i class="glyphicon glyphicon-pencil"></i> Me Cadastrar </a></li>
                        <li><a href="{$PAG_CLIENTE_DADOS}"><i class="glyphicon glyphicon-barcode"></i> Meus Dados </a>
                        </li>
                        <li><a href="{$PAG_CLIENTE_ALTERAR}"><i class="glyphicon glyphicon-edit"></i> Editar Dados </a>
                        </li>
                        <li><a href="{$PAG_CLIENTE_ALTERAR}"><i class="glyphicon glyphicon-trash"></i> Excluir Dados</a>
                        </li>
                        <li><a href=""><i class="glyphicon glyphicon-tag"></i> Produtos</a></li>
                    </ul>

                </div><!-- fim navbar collapse-->


            </div> <!--fim container navBar-->

        </nav><!-- fim da navBar-->


    </div> <!-- fim barra menu-->

    <!-- começa DIV conteudo-->
    <div class="row" id="conteudo">

        <div class="container">

            <!-- coluna direita CONYEUDO CENTRAL -->
            <div class="col-md-10">


                <ul class="breadcrumb">
                    <li><a href="#"><i class="glyphicon glyphicon-home"></i> Home </a></li>
                    <li><a href="#"> Produtos </a></li>
                    <li><a href="#"> Info </a></li>
                </ul>
                <!-- fim do menu breadcrumb-->

                {php}
                    Rotas::get_Pagina();
                    /////  var_dump(Rotas::$pag);
                {/php}

            </div>  <!--fim coluna direita-->

        </div>

    </div><!-- fim DIV conteudo-->
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <!-- começa div rodape -->
    <div class="row" id="rodape">

        <center><h3>{$SITE_NOME}</h3></center>

    </div><!-- fim div rodape-->


</div> <!-- fim do container geral -->


</body>
</html>

<h3> Excluir Dados </h3>

<!--- Excluir Dados -->
<hr>

<form name="cadcliente" id="cadcliente" method="post" action="">

    <section class="row" id="cadastro">

        <div class="col-md-4">
            <label>Nome:</label>
            <input type="text" name="cli_nome" class="form-control" required>
        </div>

        <div class="col-md-3">
            <label>Data de Nascimento:</label>
            <input type="date" name="cli_data_nascimento" class="form-control" required>
        </div>

        <div class="col-md-2">
            <label>Cod. Produto:</label>
            <input type="number" name="cli_id_produto" class="form-control" required>
        </div>

        <div class="col-md-2">
            <label>Status:</label>
            <input type="number" name="cli_status" class="form-control" required>
        </div>

    </section>

    <br>
    <br>

    <section class="row" id="btngravar">

        <div class="col-md-4"></div>

        <div class="col-md-4">
            <button type="submit" class="btn btn-geral btn-block "><i class="glyphicon glyphicon-ok"></i> Excluir Dados
            </button>
        </div>

        <div class="col-md-4"></div>

    </section>

</form>
<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Clientes' => $baseDir . '/model/Clientes.php',
    'Conexao' => $baseDir . '/model/Conexao.php',
    'Config' => $baseDir . '/model/Config.php',
    'Produtos' => $baseDir . '/model/Produtos.php',
    'Rotas' => $baseDir . '/model/Rotas.php',
    'Template' => $baseDir . '/model/Template.php',
);
